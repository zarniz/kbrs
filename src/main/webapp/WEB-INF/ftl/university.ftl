<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="eu.kielczewski.example.domain.UserCreateForm" -->
<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create a new user</title>
</head>
<body>
<nav role="navigation">
    <ul>
        <li><a href="/">Home</a></li>
    </ul>
</nav>

<h1>List university</h1>

<p>${univerity.university}</p>
<table>
    <thead>
    <tr>
        <th>Students</th>
    </tr>
    </thead>
    <tbody>
    <#list university.students as student>
    <tr>
        <td>${student.name}</td>
    </tr>
    </#list>
    </tbody>
</table>

<form role="form" name="form" action="/university/${university.id}" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div>
        <label for="student">student</label>
        <input type="text" name="student" id="student" required/>
    </div>
    <div>
        <input type="hidden" name="universityId" id="universityId" value="${university.id}" required/>
    </div>
    <button type="submit">Save</button>
</form>

</body>
</html>
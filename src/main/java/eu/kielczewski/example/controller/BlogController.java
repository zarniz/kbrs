package eu.kielczewski.example.controller;

import eu.kielczewski.example.domain.University;
import eu.kielczewski.example.domain.Student;
import eu.kielczewski.example.domain.UniversityCreateForm;
import eu.kielczewski.example.domain.validator.UserCreateFormValidator;
import eu.kielczewski.example.repository.UniversityRepository;
import eu.kielczewski.example.repository.StudentRepository;
import eu.kielczewski.example.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlogController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogController.class);
    private final UserService userService;
    private final UniversityRepository universityRepository;
    private final StudentRepository studentRepository;

    @Autowired
    public BlogController(UserService userService, UserCreateFormValidator userCreateFormValidator, UniversityRepository universityRepository, StudentRepository studentRepository) {
        this.userService = userService;
        this.universityRepository = universityRepository;
        this.studentRepository = studentRepository;
    }

    @PreAuthorize("@currentUserServiceImpl.canReadUniversity(principal, #id)")
    @RequestMapping("/blog/{id}")
    public ModelAndView getUniversityPage(@PathVariable Long id) {
        LOGGER.debug("Getting university page for user={}", id);
        return new ModelAndView("university", "university", universityRepository.getOne(id)
                );
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    @RequestMapping("/universities")
    public ModelAndView getUniversities() {
        //LOGGER.debug("Getting university page for user={}", id);
        return new ModelAndView("universities", "universities", universityRepository.findAll()
        );
    }

    @PreAuthorize("@currentUserServiceImpl.canWriteUniversity(principal, #id)")
    @RequestMapping(value = "/university/{id}", method = RequestMethod.POST)
    public String getBlogPage(@PathVariable Long id,@ModelAttribute("form") UniversityCreateForm form) {
        //LOGGER.debug("Getting university page for user={}", id);
        Student student = new Student();
        University university = new University();
        university.setId(Long.parseLong(form.getUniversityId()));
        student.setUniversity(university);
        student.setName(form.getStudent());
        studentRepository.save(student);
        return "redirect:/university/"+id;
    }

}

package eu.kielczewski.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.kielczewski.example.domain.UserHasPermission;

public interface PermissionRepository extends JpaRepository<UserHasPermission, Long> {

}

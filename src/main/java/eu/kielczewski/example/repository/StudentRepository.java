package eu.kielczewski.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.kielczewski.example.domain.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}

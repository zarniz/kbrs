package eu.kielczewski.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.kielczewski.example.domain.University;

public interface UniversityRepository extends JpaRepository<University, Long> {

}

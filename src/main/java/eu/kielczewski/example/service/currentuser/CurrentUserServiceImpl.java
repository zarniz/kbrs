package eu.kielczewski.example.service.currentuser;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.kielczewski.example.domain.CurrentUser;
import eu.kielczewski.example.domain.PermissionEnum;
import eu.kielczewski.example.domain.Role;
import eu.kielczewski.example.domain.UserHasPermission;
import eu.kielczewski.example.service.user.UserService;

@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserDetailsService.class);

    private final UserService userService;

    @Autowired
    public CurrentUserServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean canAccessUser(CurrentUser currentUser, Long userId) {
        LOGGER.debug("Checking if user={} has access to user={}", currentUser, userId);
        return currentUser != null
                && (currentUser.getRole() == Role.ADMIN || currentUser.getId().equals(userId));
    }

    @Override
    public boolean canReadBlog(CurrentUser currentUser, Long blogId) {
        LOGGER.debug("Checking if user={} has access to user={}", currentUser, blogId);
        if (currentUser != null) {
            List<UserHasPermission> permissionList = userService.getPermissions(currentUser.getId());
            return
                    (permissionList.stream().filter(it -> (it.getPk().getUniversity().getId().equals(blogId) && (it.getPermission().getName().equals(PermissionEnum.READ)||it.getPermission().getName().equals(PermissionEnum.WRITE)))).count() > 0);
        }
        return false;
    }

    @Override
    public boolean canWriteBlog(CurrentUser currentUser, Long blogId) {
        LOGGER.debug("Checking if user={} has access to user={}", currentUser, blogId);
        if (currentUser != null) {
            List<UserHasPermission> permissionList = userService.getPermissions(currentUser.getId());
            return
                    (permissionList.stream().filter(it -> (it.getPk().getUniversity().getId().equals(blogId) && it.getPermission().getName().equals(PermissionEnum.WRITE))).count() > 0);
        }
        return false;
    }

}

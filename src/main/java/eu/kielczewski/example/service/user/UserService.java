package eu.kielczewski.example.service.user;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import eu.kielczewski.example.domain.PermissionCreateForm;
import eu.kielczewski.example.domain.User;
import eu.kielczewski.example.domain.UserCreateForm;
import eu.kielczewski.example.domain.UserHasPermission;

public interface UserService {

    Optional<User> getUserById(long id);

    Optional<User> getUserByEmail(String email);

    Collection<User> getAllUsers();

    User create(UserCreateForm form);

    List<UserHasPermission> getPermissions(long id);

    UserHasPermission changePermission(PermissionCreateForm form);
}

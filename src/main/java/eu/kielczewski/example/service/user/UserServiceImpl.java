package eu.kielczewski.example.service.user;

import eu.kielczewski.example.domain.University;
import eu.kielczewski.example.domain.Permission;
import eu.kielczewski.example.domain.PermissionCreateForm;
import eu.kielczewski.example.domain.User;
import eu.kielczewski.example.domain.UserCreateForm;
import eu.kielczewski.example.domain.UserHasPermission;
import eu.kielczewski.example.domain.UserHasPermissionPK;
import eu.kielczewski.example.repository.PermissionRepository;
import eu.kielczewski.example.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final PermissionRepository permissionRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PermissionRepository permissionRepository) {
        this.userRepository = userRepository;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public Optional<User> getUserById(long id) {
        LOGGER.debug("Getting user={}", id);
        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        LOGGER.debug("Getting user by email={}", email.replaceFirst("@.*", "@***"));
        return userRepository.findOneByEmail(email);
    }

    @Override
    public Collection<User> getAllUsers() {
        LOGGER.debug("Getting all users");
        return userRepository.findAll(new Sort("email"));
    }

    @Override
    public User create(UserCreateForm form) {
        User user = new User();
        user.setEmail(form.getEmail());
        user.setPasswordHash(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setRole(form.getRole());
        return userRepository.save(user);
    }

    @Override
    public List<UserHasPermission> getPermissions(long id) {
        return userRepository.findOne(id).getUserHasPermissionList();
    }

    @Override
    public UserHasPermission changePermission(PermissionCreateForm form) {
        List<UserHasPermission> permissio = permissionRepository.findAll();
        Optional<UserHasPermission> perm = permissio.stream().filter(
                it->
                        (it.getPk().getUniversity().getId()==Long.parseLong(form.getUniversityId()))&&it.getPk().getUser().getId()==Long.parseLong(form.getUserId())

        ).findAny();
        if(perm.isPresent()){
            permissionRepository.delete(perm.get());
        }
        UserHasPermission userHasPermission = new UserHasPermission();
        UserHasPermissionPK userHasPermissionPK = new UserHasPermissionPK();
        User user = new User();
        user.setId(Long.parseLong(form.getUserId()));
        Permission permission = new Permission();
        permission.setId(Long.parseLong(form.getPermissionId()));

        University university = new University();
        userHasPermissionPK.setUniversity(university);
        userHasPermissionPK.setUser(user);
        userHasPermission.setPk(userHasPermissionPK);
        university.setId(Long.parseLong(form.getUniversityId()));
        userHasPermission.setPermission(permission);
        return permissionRepository.save(userHasPermission);
    }

}

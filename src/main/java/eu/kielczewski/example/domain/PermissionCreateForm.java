package eu.kielczewski.example.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class PermissionCreateForm {

    @NotEmpty
    private String universityId = "";

    @NotEmpty
    private String userId = "";

    @NotEmpty
    private String permissionId = "";

    public String getUniversityId() {
        return universityId;
    }

    @Override
    public String toString() {
        return "PermissionCreateForm{" +
                "universityId='" + universityId + '\'' +
                ", userId='" + userId + '\'' +
                ", permissionId='" + permissionId + '\'' +
                '}';
    }

    public void setUniversityId(String universityId) {
        this.universityId = universityId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
}

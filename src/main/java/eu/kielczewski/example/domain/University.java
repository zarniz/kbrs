package eu.kielczewski.example.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "university")
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "university", nullable = false, unique = true)
    private String university;

    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    private List<Student> students;

    public Long getId() {
        return id;
    }

  @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", university='" + university +
                '}';
    }

    public void setUniversity(String name) {
        this.university = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniversity() {
        return university;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}

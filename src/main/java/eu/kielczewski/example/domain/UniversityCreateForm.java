package eu.kielczewski.example.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class UniversityCreateForm {

    @NotEmpty
    private String universityId = "";

    @NotEmpty
    private String student = "";

    public String getUniversityId() {
        return universityId;
    }

    public void setUniversityId(String blogId) {
        this.universityId = blogId;
    }

    public String getStudent() {
        return student;
    }

    @Override
    public String toString() {
        return "UniversityCreateForm{" +
                "universityId='" + universityId + '\'' +
                ", student='" + student + '\'' +
                '}';
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
